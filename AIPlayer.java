//Name: Luke Dixon
//ID: 1603069
package TicTacToe5x5;

//Board.java has been edited to allow for a 5x5 board. The only changes made were making sure it took the new points on the board into account

class AIPlayer {
    public Point Move(Board b) {
        Board Child;
        Point rtn = null;
        int BestScore = Integer.MIN_VALUE;
        int CurrentScore;

        //Iterates through all child nodes of current node
        for (Point p : b.getAvailablePoints()) {
            Child = new ChildBoard(b);
            Child.placeAMove(p, 1);
            //Runs minimax on current child node, max depth 3. Note on my hardware '3' was optimal however this may be different on other hardware.
            CurrentScore = Minimax(Child, 3, Integer.MIN_VALUE, Integer.MAX_VALUE, false);
            //Out of all iterations, works out the best possible score, and therefore best possible route.
            if (BestScore < CurrentScore) {
                rtn = p;
                BestScore = CurrentScore;
            }
        }
        return rtn;
    }

    //Pseudo code provided by https://en.wikipedia.org/wiki/Minimax and https://en.wikipedia.org/wiki/Alpha–beta_pruning has been used as a base to create this method, however it has been heavily modified
    //Recursive function, takes a copy of the current child board, and whether to maximise or minimise this iteration. depth, alpha and beta have been added since 3x3 to allow for setting a maximum depth and for alpha-beta pruning to take place.
    private int Minimax(Board b, int depth, int alpha, int beta, boolean max) {
        int rtn;
        Board Child;

        //Game ended or max depth reached
        if (b.getAvailablePoints().isEmpty() || depth == 0)
            return CalculateBoardValue(b); //return heuristic value of board


        //Time to maximise
        if (max) {
            rtn = Integer.MIN_VALUE;
            for (Point p : b.getAvailablePoints()) {
                //Goes through all possible child nodes of current node
                Child = new ChildBoard(b);
                Child.placeAMove(p, 1);
                //Minimax's on the new child
                rtn = Math.max(rtn, Minimax(Child, depth - 1, alpha, beta, false));
                //Stores the highest value returned from all child nodes until depth = 0
                alpha = Math.max(alpha, rtn);
                //Alpha-beta pruning:
                if (beta <= alpha) break;
            }
        } else {
            //Time to minimise (same as max unless noted)
            rtn = Integer.MAX_VALUE;
            for (Point p : b.getAvailablePoints()) {
                Child = new ChildBoard(b);
                Child.placeAMove(p, 2);
                rtn = Math.min(rtn, Minimax(Child, depth - 1, alpha, beta, true));
                //Stores the smallest value returned from all child nodes until depth = 0
                beta = Math.min(beta, rtn);
                //Alpha-beta pruning:
                if (beta <= alpha) break;
            }
        }
        return rtn;
    }


    //Heuristic function
    //Logic found on https://www.ics.uci.edu/~rickl/courses/cs-171/cs171-lecture-slides/cs-171-07-Games.pdf however all code provided by myself
    //Board value found by calculating number of possible wins - number of possible losses (a.k.a. number of possible opponent wins) at current node
    private int CalculateBoardValue(Board b) {
        return CalculatePossibleWins(b, 2) - CalculatePossibleWins(b, 1);
    }

    private int CalculatePossibleWins(Board b, int Opponent) {
        int PossibleWins = 0;

        //Reached terminal node, send back large int to prioritise acting on this line (to win or prevent loss)
        if ((b.hasXWon() && Opponent == 2) || b.hasOWon() && Opponent == 1) return Integer.MAX_VALUE;

        //Calculate possible wins (columns)
        for (int i = 0; i < b.board[0].length; i++) {
            PossibleWins++;
            for (int j = 0; j < b.board[0].length; j++) {
                if (b.board[i][j] == Opponent) {
                    //Can't win on this line
                    PossibleWins--;
                    break;
                }
            }
        }
        //Calculate possible wins (rows)
        for (int i = 0; i < b.board[0].length; i++) {
            PossibleWins++;
            for (int j = 0; j < b.board[0].length; j++) {
                if (b.board[j][i] == Opponent) {
                    //Can't win on this line
                    PossibleWins--;
                    break;
                }
            }
        }
        //Calculate possible wins (diagonals)
        PossibleWins++;
        for (int k = 0; k < b.board[0].length; k++) {
            if (b.board[k][k] == Opponent) {
                //Can't win on this line
                PossibleWins--;
                break;
            }
        }
        PossibleWins++;
        for (int k = 0; k < b.board[0].length; k++) {
            if (b.board[k][(b.board[0].length - 1) - k] == Opponent) {
                //Can't win on this line
                PossibleWins--;
                break;
            }
        }
        return PossibleWins;
    }
}

//Exclusively for creating child nodes without editing the parent
class ChildBoard extends Board {
    //Copy constructor to stop the pointer of board being sent when messing with child nodes.
    public ChildBoard(Board b) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                //Simply copies the original board into the current instance's board
                this.board[i][j] = b.board[i][j];
            }
        }
    }
}