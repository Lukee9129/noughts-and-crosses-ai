//Name: Luke Dixon
//ID: 1603069
package TicTacToe5x5;//Commenting is not done deliberately

import java.util.Random;

public class TicTacToe {
    public static void main(String[] args) {


        Board b = new Board();
        Point p = new Point(0, 0);
        //Creating my AI:
        AIPlayer CPU = new AIPlayer();
        Random rand = new Random();
        b.displayBoard();

        System.out.println("Who makes first move? (1)Computer (2)User: ");
        int choice = b.scan.nextInt();
        if(choice == 1){
            p.x = rand.nextInt(5);
            p.y = rand.nextInt(5);
            b.placeAMove(p, 1);
            b.displayBoard();
        }

        while (!b.isGameOver()) {

            System.out.println("Your move: line (1, 2, 3, 4 or 5) column (1, 2, 3, 4 or 5)");
            Point userMove = new Point(b.scan.nextInt()-1, b.scan.nextInt()-1);
	    while (b.getState(userMove)!=0) {
	    	System.out.println("Invalid move. Make your move again: ");
	    	userMove.x=b.scan.nextInt()-1;
	    	userMove.y=b.scan.nextInt()-1;
	    }
            b.placeAMove(userMove, 2);  
            b.displayBoard();
            
            if (b.isGameOver()) {
                break;
            }
            //In the following do loop, Computer makes random moves. Your assignment is to replace it by implementing 
            //an AIplayer class that chooses the best moves based on minimax search.
            //do {
	        //p.x = rand.nextInt(5);
	        //p.y = rand.nextInt(5);
	        //} while (b.getState(p)!=0);

            //Work out next best point to play for AI
            p = CPU.Move(b);

            b.placeAMove(p, 1); 
            b.displayBoard();
        }
        if (b.hasXWon()) {
            System.out.println("Unfortunately, you lost!");
        } else if (b.hasOWon()) {
            System.out.println("You win!");
        } else {
            System.out.println("It's a draw!");
        }
    }
    
}